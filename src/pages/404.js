import React from 'react'

import Layout from '../components/layout'

const NotFoundPage = () => (
  <Layout>
    <h1>404</h1>
    <p>Cette page n'existe pas encore. Ca craint :(</p>
  </Layout>
)

export default NotFoundPage
